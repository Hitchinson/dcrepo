{
    "id": "8f6d053d-9bcd-4998-b003-35d20936056a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemyRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6e9e6405-b58d-4ba0-9a1b-99e5e39e92be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f6d053d-9bcd-4998-b003-35d20936056a",
            "compositeImage": {
                "id": "19587d48-f44b-4e67-ad1d-07b48bd4ea93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e9e6405-b58d-4ba0-9a1b-99e5e39e92be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e279f383-36c4-42c7-9205-214b3da1ff3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e9e6405-b58d-4ba0-9a1b-99e5e39e92be",
                    "LayerId": "bb087e9d-ebfb-436b-aefd-329e8401be4d"
                }
            ]
        },
        {
            "id": "88f946c0-b466-42c0-9d96-379adc6e5e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f6d053d-9bcd-4998-b003-35d20936056a",
            "compositeImage": {
                "id": "84ab45f2-4528-44e5-bf18-49c62509179a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f946c0-b466-42c0-9d96-379adc6e5e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c361749-abc2-405f-84e6-59b5fc48b84e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f946c0-b466-42c0-9d96-379adc6e5e02",
                    "LayerId": "bb087e9d-ebfb-436b-aefd-329e8401be4d"
                }
            ]
        },
        {
            "id": "e29deafc-889d-4988-98b7-84b8b254476f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f6d053d-9bcd-4998-b003-35d20936056a",
            "compositeImage": {
                "id": "0a97b7a5-b17d-4ef4-9f3d-bab09eec1630",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e29deafc-889d-4988-98b7-84b8b254476f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c817e18-2367-4913-9561-20d4bce856c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e29deafc-889d-4988-98b7-84b8b254476f",
                    "LayerId": "bb087e9d-ebfb-436b-aefd-329e8401be4d"
                }
            ]
        },
        {
            "id": "6ba0780d-29d6-4c1c-b942-0572cb2cbc64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f6d053d-9bcd-4998-b003-35d20936056a",
            "compositeImage": {
                "id": "e2f54f23-b49a-4464-bbe2-ad093bd78ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba0780d-29d6-4c1c-b942-0572cb2cbc64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6988b217-2c0a-49c2-bcbe-a4a388218168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba0780d-29d6-4c1c-b942-0572cb2cbc64",
                    "LayerId": "bb087e9d-ebfb-436b-aefd-329e8401be4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "bb087e9d-ebfb-436b-aefd-329e8401be4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f6d053d-9bcd-4998-b003-35d20936056a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}