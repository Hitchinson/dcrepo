{
    "id": "0e414b03-1358-4f59-943f-87cf886522f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemyDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 12,
    "bbox_right": 42,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dec38eb2-6a81-49e9-9cf5-e3dc44702748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "compositeImage": {
                "id": "06bb333a-8374-4517-a02d-e935f48a3066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec38eb2-6a81-49e9-9cf5-e3dc44702748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60a5b263-9910-4838-b513-4d869561773f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec38eb2-6a81-49e9-9cf5-e3dc44702748",
                    "LayerId": "66e05132-3339-4dd8-8c02-984c9e789400"
                }
            ]
        },
        {
            "id": "b533bb14-4e04-4ce5-a8ae-31b054519ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "compositeImage": {
                "id": "fb13d1da-4fe1-4b16-9f8f-86ca2e290c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b533bb14-4e04-4ce5-a8ae-31b054519ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52be1df6-48ca-479c-83ec-952bae8f0c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b533bb14-4e04-4ce5-a8ae-31b054519ae4",
                    "LayerId": "66e05132-3339-4dd8-8c02-984c9e789400"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "66e05132-3339-4dd8-8c02-984c9e789400",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}