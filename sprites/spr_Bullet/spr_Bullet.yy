{
    "id": "46631b9d-1363-417e-aec5-cb06aeda8edd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "94d8068b-aadb-42de-b300-1d9fa1fd3b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46631b9d-1363-417e-aec5-cb06aeda8edd",
            "compositeImage": {
                "id": "75a0e2bc-3923-4687-b2b5-c190838eb9ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d8068b-aadb-42de-b300-1d9fa1fd3b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3efa5aea-a182-445f-a62b-ad89d35838d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d8068b-aadb-42de-b300-1d9fa1fd3b99",
                    "LayerId": "f579a0b2-ed1a-4177-a124-fb3abd39d6fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "f579a0b2-ed1a-4177-a124-fb3abd39d6fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46631b9d-1363-417e-aec5-cb06aeda8edd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 51,
    "xorig": 25,
    "yorig": 6
}