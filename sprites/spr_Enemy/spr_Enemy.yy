{
    "id": "378c1f2d-818d-453b-9302-6cf11be1da31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ba5700e9-f9f2-4090-a71d-002c5f5f226c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "8fdace02-9ac8-4a04-ad17-601ac9532845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba5700e9-f9f2-4090-a71d-002c5f5f226c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a51e1ea7-3248-4d44-9dd0-354ae3513d94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba5700e9-f9f2-4090-a71d-002c5f5f226c",
                    "LayerId": "0b9e2dd4-eef8-44c5-bf31-c0b264be8c94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0b9e2dd4-eef8-44c5-bf31-c0b264be8c94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}