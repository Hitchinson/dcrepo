{
    "id": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0217a9cb-843d-403f-aa3e-d1f255912b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
            "compositeImage": {
                "id": "dfed6690-fbb1-4e94-811b-bff2931554f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0217a9cb-843d-403f-aa3e-d1f255912b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e0450d-0005-4e51-97ac-1123b44f1373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0217a9cb-843d-403f-aa3e-d1f255912b20",
                    "LayerId": "5326c1fd-92d8-4575-8ce2-2916a6398ca6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "5326c1fd-92d8-4575-8ce2-2916a6398ca6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}