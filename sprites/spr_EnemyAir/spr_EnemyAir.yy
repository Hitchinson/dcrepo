{
    "id": "6c0ed852-2cef-491e-8c47-db10952cd05c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemyAir",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6c9d9265-3565-4ff4-bf11-0f10b862b8f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c0ed852-2cef-491e-8c47-db10952cd05c",
            "compositeImage": {
                "id": "de700364-b9cb-4a36-ab4b-761e9567a456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9d9265-3565-4ff4-bf11-0f10b862b8f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ade302a-372b-4d18-a9d3-6af33b4cf5bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9d9265-3565-4ff4-bf11-0f10b862b8f4",
                    "LayerId": "7f92b739-01bc-4d18-bd4a-ccd766925ae1"
                }
            ]
        },
        {
            "id": "4e6f9db0-1580-4315-89ff-64882a4a1f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c0ed852-2cef-491e-8c47-db10952cd05c",
            "compositeImage": {
                "id": "3cd32731-2b60-4175-8676-59a31cc66ac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6f9db0-1580-4315-89ff-64882a4a1f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbc35e95-b202-4117-90f9-979348d68bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6f9db0-1580-4315-89ff-64882a4a1f27",
                    "LayerId": "7f92b739-01bc-4d18-bd4a-ccd766925ae1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7f92b739-01bc-4d18-bd4a-ccd766925ae1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c0ed852-2cef-491e-8c47-db10952cd05c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}