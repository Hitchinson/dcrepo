{
    "id": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 3,
    "bbox_right": 84,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "27563db1-88b8-4e37-9d86-5ba91e34df27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "compositeImage": {
                "id": "254cf49f-c16d-4f30-af19-6945213b0e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27563db1-88b8-4e37-9d86-5ba91e34df27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c80fc23e-8d7c-4948-b88f-9f19dd2d0e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27563db1-88b8-4e37-9d86-5ba91e34df27",
                    "LayerId": "d7d23a34-4af0-4dfa-b27f-707510bc2e0d"
                }
            ]
        },
        {
            "id": "6a13b2d7-87c2-4633-bc38-22c3194bc723",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "compositeImage": {
                "id": "a2c192d0-5bd2-41eb-ad58-bbe729e94846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a13b2d7-87c2-4633-bc38-22c3194bc723",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36082dff-bad2-4c90-9e9d-d4c47798fff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a13b2d7-87c2-4633-bc38-22c3194bc723",
                    "LayerId": "d7d23a34-4af0-4dfa-b27f-707510bc2e0d"
                }
            ]
        },
        {
            "id": "2941c443-e0d6-489b-b4d5-8829a867d9c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "compositeImage": {
                "id": "df7dc2cf-54e5-4013-9230-d977e259fef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2941c443-e0d6-489b-b4d5-8829a867d9c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ac41685-76fc-4ef8-9761-8777b91395a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2941c443-e0d6-489b-b4d5-8829a867d9c0",
                    "LayerId": "d7d23a34-4af0-4dfa-b27f-707510bc2e0d"
                }
            ]
        },
        {
            "id": "dd43d1cc-f86a-4cfa-bfdf-cb0fbbd82241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "compositeImage": {
                "id": "34bff9ac-83d6-4c0c-9d52-75e18438d354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd43d1cc-f86a-4cfa-bfdf-cb0fbbd82241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33130db6-f624-47fe-8ad3-6c199bd51911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd43d1cc-f86a-4cfa-bfdf-cb0fbbd82241",
                    "LayerId": "d7d23a34-4af0-4dfa-b27f-707510bc2e0d"
                }
            ]
        },
        {
            "id": "51102bb3-7bcd-442f-beaf-fdb3cb1bf5f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "compositeImage": {
                "id": "f1882e15-3a3a-42a4-a015-b9832afbc4e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51102bb3-7bcd-442f-beaf-fdb3cb1bf5f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bd8d25-1af3-40b3-a655-61221f408c5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51102bb3-7bcd-442f-beaf-fdb3cb1bf5f0",
                    "LayerId": "d7d23a34-4af0-4dfa-b27f-707510bc2e0d"
                }
            ]
        },
        {
            "id": "c1e24a31-d54f-42fa-bc33-d7905602aaac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "compositeImage": {
                "id": "b6ac0bcd-c301-4788-859b-95d070b7f212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e24a31-d54f-42fa-bc33-d7905602aaac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "109100dd-5973-4055-9b5b-445b8c7d87dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e24a31-d54f-42fa-bc33-d7905602aaac",
                    "LayerId": "d7d23a34-4af0-4dfa-b27f-707510bc2e0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d7d23a34-4af0-4dfa-b27f-707510bc2e0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 32
}