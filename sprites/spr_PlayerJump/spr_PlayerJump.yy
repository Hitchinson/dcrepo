{
    "id": "2976a37f-0260-46cb-a367-09d7eea8de49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 0,
    "bbox_right": 194,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "94d9e3e6-7f93-469c-9c40-2ca7e504a487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "ef253423-4a6a-480d-b9a4-58edebf21a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d9e3e6-7f93-469c-9c40-2ca7e504a487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddf52d45-7c64-4e6a-bfd3-5477a76013a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d9e3e6-7f93-469c-9c40-2ca7e504a487",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        },
        {
            "id": "e6af2fda-4950-416a-8a66-f4e941e895f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "8ab02a07-de4b-43d5-be59-556fa2b4fcdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6af2fda-4950-416a-8a66-f4e941e895f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5acb8bdf-a003-4705-9efc-04d10918accd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6af2fda-4950-416a-8a66-f4e941e895f1",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        },
        {
            "id": "0d814e4b-aa82-46bd-a43e-faeb230f7c0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "8844fca0-a452-4a4d-b839-45136f3828f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d814e4b-aa82-46bd-a43e-faeb230f7c0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f563a6-2e6d-4340-be79-9a4aed16d0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d814e4b-aa82-46bd-a43e-faeb230f7c0c",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        },
        {
            "id": "d205f2c9-0530-4dd2-b0b6-4fea68fbaa37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "78730503-1260-4472-9687-0994060c2e1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d205f2c9-0530-4dd2-b0b6-4fea68fbaa37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1eecaef-e20c-4d4c-9204-51e0605a6825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d205f2c9-0530-4dd2-b0b6-4fea68fbaa37",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        },
        {
            "id": "342c764e-8791-46b1-bb82-7ef633d46cdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "dd7ceeef-8891-4052-82c6-8cc6f5330b5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342c764e-8791-46b1-bb82-7ef633d46cdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbf11c23-99f9-434f-8553-f95e454ff73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342c764e-8791-46b1-bb82-7ef633d46cdb",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        },
        {
            "id": "85bddbef-385b-40aa-9f35-3ca44ae13340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "cf442ec9-b6db-46c2-8c40-a0c7f935e78b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bddbef-385b-40aa-9f35-3ca44ae13340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25714ea4-88bc-4ac7-b078-83e6ece611da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bddbef-385b-40aa-9f35-3ca44ae13340",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        },
        {
            "id": "d62e74e0-8efc-4b49-ad0f-3b0c10b6f76e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "1303faaf-27ae-4bcb-87ec-c36dab7f0dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62e74e0-8efc-4b49-ad0f-3b0c10b6f76e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "287a55bf-4b3f-4b8c-bba9-d771402a287f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62e74e0-8efc-4b49-ad0f-3b0c10b6f76e",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        },
        {
            "id": "9f05e94c-eb77-4d68-a87d-d15bfc0f7bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "compositeImage": {
                "id": "ff6cb2e4-38b2-4bc1-bdd9-68d3e6093bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f05e94c-eb77-4d68-a87d-d15bfc0f7bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8c63ee-2f40-4d78-b1c6-30b76e6c1973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f05e94c-eb77-4d68-a87d-d15bfc0f7bc5",
                    "LayerId": "bfcb24ac-1ef4-49dc-badc-832e859589dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 124,
    "layers": [
        {
            "id": "bfcb24ac-1ef4-49dc-badc-832e859589dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2976a37f-0260-46cb-a367-09d7eea8de49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 195,
    "xorig": 97,
    "yorig": 62
}