//Get Player Input
key_left = keyboard_check(ord("A")) or keyboard_check(vk_left);
key_right = keyboard_check(ord("D")) or keyboard_check(vk_right);
key_jump = keyboard_check_pressed(vk_space);
key_flight = keyboard_check(vk_lshift);

//Calculate Movement
var move = key_right - key_left;

hsp = move * walksp;

vsp = vsp + grv;


if  (place_meeting(x,y+1,obj_Wall)) and (key_jump)
{
	vsp = -15;
	audio_play_sound(aud_Jump,10,false);
}

if  (key_flight)
{
	vsp = -1;
	y = y - 1;

}

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_Wall))
{
	while (!place_meeting(x+sign(hsp),y,obj_Wall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}

x = x + hsp;

//Vertical Collision
if (place_meeting(x,y+vsp,obj_Wall))
{
	while (!place_meeting(x,y+sign(vsp),obj_Wall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

y = y + vsp;

//Animation

if (!place_meeting(x,y+1,obj_Wall))
{

	sprite_index = spr_Player;
	image_speed = 0;
	if (sign(vsp) > 0) and key_flight = false
	
		{	
		sprite_index = spr_PlayerJump; 
		image_speed = 1;
		}
		
		
	else 
	{
		sprite_index = spr_PlayerDrop; 
		image_speed = 1;	
	}
	
}
else
{
	
	if (hsp == 0)
	{
		image_speed = 0;
		sprite_index = spr_Player;
		image_index = 5;
	}
		else
		{
		image_speed = 1;
		sprite_index = spr_Player;
		}
	}	

if (hsp !=0) image_xscale = sign(hsp);

if (key_flight)
	{	sprite_index = spr_PlayerFlight; 
		image_speed = 1;
	}
//Parallax Backgrounds
var x_cam = camera_get_view_x(view_camera[0]);
layer_x("SkyBG", x_cam * 0.5);
layer_x("SecondaryBG", x_cam * 0.3);









