{
    "id": "8f71074c-f59e-44e2-b1f3-ecb84be90b37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bullet",
    "eventList": [
        {
            "id": "4ec001e7-8eae-4a7c-9fd7-50b7b98807b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8f71074c-f59e-44e2-b1f3-ecb84be90b37"
        },
        {
            "id": "f31b0d3a-a174-4883-93f2-57afd9231bad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8f71074c-f59e-44e2-b1f3-ecb84be90b37"
        },
        {
            "id": "6d5562de-f961-47d7-a6d3-288c5453c8ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8f71074c-f59e-44e2-b1f3-ecb84be90b37"
        },
        {
            "id": "3e361e07-1412-4767-b050-e365ca76860f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f71074c-f59e-44e2-b1f3-ecb84be90b37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "46631b9d-1363-417e-aec5-cb06aeda8edd",
    "visible": true
}