
{
    "name": "rm_2",
    "id": "53e42354-41a3-48e2-92eb-853cadc05d24",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "9aea65d8-176c-4d27-97d0-2f498c57c83e",
        "d9b692aa-795d-4626-ae0a-d75b32a8030f",
        "6f8b00ec-a845-4501-bf5a-279dc3cecba6",
        "bfcd46a4-10b2-4cbb-b33b-b73e62626af3",
        "a9730a6a-c75b-471b-8cd9-b967e748512d",
        "e355d2a9-940c-4e20-8b36-3e7c11151108",
        "40041daa-1ba6-43ce-ae00-e33a50188372",
        "a0094dd8-6bdb-438c-892e-eb6a8058934c",
        "bf6303e1-c37b-4a97-94fa-84c11783c314",
        "bb6bd982-dda3-463f-8bc9-04b82f1288e3",
        "f9396f4f-bb87-4c36-87e6-8b3ebe02f095",
        "3d5e4ef0-48db-41bc-9823-284b64ad7c19",
        "9c0e0280-8ec4-42af-a14c-0d08de24a4b0"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Camera",
            "id": "9e5e374a-5279-445d-849e-ddfded360a02",
            "depth": -600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
                {"name": "inst_E1045B6","id": "bfcd46a4-10b2-4cbb-b33b-b73e62626af3","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_E1045B6.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_E1045B6","objId": "1356a7b2-b019-44cf-8eb2-dbf6446d4c71","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 960,"y": 1600}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Bullets",
            "id": "8c71e543-8ffc-4059-b47f-d91a2e6ccc98",
            "depth": -500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Gun",
            "id": "6f661757-e950-4fac-a16b-3bd2eb833295",
            "depth": -400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
                {"name": "inst_BDE16C1","id": "d9b692aa-795d-4626-ae0a-d75b32a8030f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_BDE16C1","objId": "c66a162f-b6f0-4551-a224-7001996fc06f","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 1216,"y": 1504}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Player",
            "id": "04ff8634-da7a-4984-9403-2a15ab5a7095",
            "depth": -300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
                {"name": "inst_11BDC503","id": "9aea65d8-176c-4d27-97d0-2f498c57c83e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_11BDC503","objId": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 1504,"y": 480}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Enemy",
            "id": "b8e1f871-4575-49d9-be0a-6c6bfcd140d6",
            "depth": -200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Platforms",
            "id": "acb017b6-4127-4327-8fa3-dc2d1afba8de",
            "depth": -100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
                {"name": "inst_46B98A27","id": "40041daa-1ba6-43ce-ae00-e33a50188372","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_46B98A27","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 49,"scaleY": 7.4,"mvc": "1.0","x": 672,"y": 1536},
                {"name": "inst_762BB6CD","id": "a0094dd8-6bdb-438c-892e-eb6a8058934c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_762BB6CD","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 49,"scaleY": 7.4,"mvc": "1.0","x": 1408,"y": 1376},
                {"name": "inst_61977047","id": "bf6303e1-c37b-4a97-94fa-84c11783c314","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_61977047","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 49,"scaleY": 7.4,"mvc": "1.0","x": 1920,"y": 1728},
                {"name": "inst_5DD8D948","id": "bb6bd982-dda3-463f-8bc9-04b82f1288e3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5DD8D948","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 49,"scaleY": 7.4,"mvc": "1.0","x": 2144,"y": 1280},
                {"name": "inst_417012F0","id": "f9396f4f-bb87-4c36-87e6-8b3ebe02f095","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_417012F0","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 49,"scaleY": 7.4,"mvc": "1.0","x": 3264,"y": 1440},
                {"name": "inst_7B60E782","id": "3d5e4ef0-48db-41bc-9823-284b64ad7c19","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7B60E782","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 49,"scaleY": 7.4,"mvc": "1.0","x": 2560,"y": 1600},
                {"name": "inst_4E27F465","id": "9c0e0280-8ec4-42af-a14c-0d08de24a4b0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4E27F465","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 343.4,"scaleY": 7.4,"mvc": "1.0","x": 4352,"y": 1024}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Walls",
            "id": "0ecb2283-8c17-4e42-8f7d-4393e96eed34",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
                {"name": "inst_34AC5CEE","id": "6f8b00ec-a845-4501-bf5a-279dc3cecba6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_34AC5CEE","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 1197.8,"scaleY": 7.4,"mvc": "1.0","x": 5984,"y": 1888},
                {"name": "inst_82C3EAD","id": "a9730a6a-c75b-471b-8cd9-b967e748512d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_82C3EAD","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 1,"scaleY": 42.6,"mvc": "1.0","x": 0,"y": 1638},
                {"name": "inst_4F66992E","id": "e355d2a9-940c-4e20-8b36-3e7c11151108","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4F66992E","objId": "0f8375ae-4aa3-42b6-96d4-e00083262bb3","rotation": 0,"scaleX": 1,"scaleY": 42.6,"mvc": "1.0","x": 11968,"y": 1632}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": true,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "PrimaryBG",
            "id": "4e8db64e-bbe0-417a-9ddb-a7944ddacdae",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 960,
            "prev_tilewidth": 3000,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 2,
                "SerialiseWidth": 4,
                "TileSerialiseData": [
                    4,5,6,7,
                    8,9,10,11
                ]
            },
            "tilesetId": "61937c0d-0f25-4451-bab1-2a91206c2075",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "SecondaryBG",
            "id": "fafe285a-c8db-4ae5-a768-28c51ca8783a",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 960,
            "prev_tilewidth": 3000,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 2,
                "SerialiseWidth": 4,
                "TileSerialiseData": [
                    4,5,6,7,
                    8,9,10,11
                ]
            },
            "tilesetId": "89df6766-2e4c-477e-8f4d-e8e9a177db21",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "SkyBG",
            "id": "88ef9bdc-c8e1-4328-bbf0-59ac01def474",
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 960,
            "prev_tilewidth": 3000,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 2,
                "SerialiseWidth": 4,
                "TileSerialiseData": [
                    4,5,6,7,
                    8,9,10,11
                ]
            },
            "tilesetId": "3338c100-7c95-4219-b6f1-d619d5250917",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "bae4506d-4090-4aa0-a3a6-6be48cff6d95",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "1cd3b52a-abf8-45ae-82a2-21bc7fcaf39e",
        "Height": 1920,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 12000
    },
    "mvc": "1.0",
    "views": [
        {"id": "dcdedd27-545a-4471-b672-0ce9f48ebfbb","hborder": 1920,"hport": 1080,"hspeed": -1,"hview": 1080,"inherit": false,"modelName": "GMRView","objId": "c66a162f-b6f0-4551-a224-7001996fc06f","mvc": "1.0","vborder": 0,"visible": true,"vspeed": -1,"wport": 1920,"wview": 1920,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "4a0e9694-1cb1-499a-b9d4-be8ab1027696","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "e78be9cb-d431-4f4b-9996-f4450815298d","hborder": 700,"hport": 1080,"hspeed": 10,"hview": 1080,"inherit": false,"modelName": "GMRView","objId": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12","mvc": "1.0","vborder": 400,"visible": true,"vspeed": 10,"wport": 1920,"wview": 1920,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "837ae8db-0c4a-48d8-a489-f8081ef3c6d5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "084099f5-56b6-4eb1-9f29-5ac324734290","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "2be6d2d3-2894-49bc-8aba-b0c2acd22349","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "3783e85d-97d5-4502-a123-8e8d631945f1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "27e65db5-d29a-4dff-883e-2bd2251ff909","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "b90e663b-6656-459c-99c8-e46fe79dfb1c",
        "clearDisplayBuffer": true,
        "clearViewBackground": true,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}